import random
import pandas as pd

def extract_subset_of_data(filename, p):
    df = pd.read_csv(
        filename,
        header=0, 
        skiprows=lambda i: i>0 and random.random() > p,
        low_memory=False
    )
    return df
