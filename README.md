# FakeNewsDetection project

In order to run models run the following, where `<SCRIPT>` is model name.

```
./bin/database reset
./bin/model <SCRIPT>
```

To see models check out the `./lib/model/models` directory.

## Directory structure

`bin` contains executable scripts.

- `./bin/data` controls the large raw data (data pipeline).
- `./bin/database` manages data-integration and the database (data pipeline).
- `./bin/model` manages models / evaluates models (model pipeline).
- `./bin/testds` runs simple tests.

`data` contains data.

`lib` contains the source code.

- `lib/data` is code related to data management and extraction. 
- `lib/dataintegration` is code related cleaning and formatting for the database.
- `lib/db` is code related to interacting with the database.
- `lib/model` is model pipeline and models.
- **`lib/model/models` is all the models**.
- `lib/scraping` is the scraping scripts for wikinews.
- `main.py` contains all main-functions.

`outputs`: outputs from running expensive operations, such as testing.

`scripts`: conda env management

`database.ini`: database configuration

`environment.yml`: conda configuration

## conda setup

We use conda.

In bash run the following commands to manage the conda environment.

```bash
source ./scripts/setup
source ./scripts/update
source ./scripts/activate
source ./scripts/deactivate
source ./scripts/notebookserver
```

See: `https://towardsdatascience.com/managing-project-specific-environments-with-conda-406365a539ab`

## direnv
We use `direnv` for convenience.

`direnv` allows to set/unset environment variables when entering directories.

Check the `.envrc` file for details. 
