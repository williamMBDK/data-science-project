import numpy as np
import pandas as pd
from sklearn import metrics

from .model import Model
from ..data import DataContainer, Types

class ModelEvaluator:
    
    def __init__(self, model):
        self.model = model
        self.predictions = None
        self.real = None

    def init(self, input : pd.DataFrame, output : pd.Series):
        self.predictions = (pd.Series([
            self.model.predict(row)
            for _, row in input.iterrows()
        ], name="type") == Types.REAL).to_numpy()
        self.real = (output == Types.REAL).to_numpy()

    def accuracy(self):
        return metrics.accuracy_score(self.real, self.predictions)

    def precision(self):
        try:
            return metrics.precision_score(self.real, self.predictions)
        except Exception as e:
            return str(e)

    def recall(self):
        try:
            return metrics.recall_score(self.real, self.predictions)
        except Exception as e:
            return str(e)

    def f1_score(self):
        try:
            return metrics.f1_score(self.real, self.predictions)
        except Exception as e:
            return str(e)

    def roc_auc(self):
        try:
            return metrics.roc_auc_score(self.real, self.predictions)
        except Exception as e:
            return str(e);

    def summary(self, data_description):
        print("="*5+"Model-Evaluation"+"="*5)
        print("description: {}".format(data_description))
        print("model: {}".format(self.model.getname()))
        print("accuracy: {}".format(self.accuracy()))
        print("precision: {}".format(self.precision()))
        print("recall: {}".format(self.recall()))
        print("f1 score: {}".format(self.f1_score()))
        print("area under ROC curve: {}".format(self.roc_auc()))
        print("="*26)

def run_model_pipeline(model : Model, data=None, columns=["content"]):
    if data == None:
        data = DataContainer(columns=columns)
        data.init_default()
        data.get_all_stats()
    model.train(data.train_in, data.train_out)
    evaluator = ModelEvaluator(model)
    evaluator.init(data.val_in, data.val_out)
    evaluator.summary("fakenewscorpus and wikinews datasets")
