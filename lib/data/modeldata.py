import pandas as pd
from random import Random
from sklearn.model_selection import train_test_split
from lib.dataintegration.cleaning import process_text

from ..db import query

class Types:
    FAKE="FAKE"
    REAL="REAL"

GENERATE_SEED = 7
DB_GENERATED_SEED = GENERATE_SEED / 10000
RD = Random()
RD.seed(GENERATE_SEED)

class DataContainer:
    def __init__(self, columns=["content"]):
        self.columns = columns
        self.train_in = None
        self.train_out = None
        self.val_in = None
        self.val_out = None
        self.test_in = None
        self.test_out = None
        self.liar_in = None
        self.liar_out = None
    
    def get_db_size(self):
        return int(query("select count(*) from training_articles")[0][0])
    
    def init_liar(self):
        df = pd.read_csv(
            "./data/liar/all.tsv",
            low_memory=False,
            sep="\t"
        )
        # title is subject
        df.columns = ['id', 'type', 'content', "title", "speaker", "jobtitle", "state", "party", "btc", "fc", "htc", "mtc", "pofc", "context"]
        df = df[["type", "content", "title"]]
        fake_rows = []
        real_rows = []
        for idx, row in df.iterrows():
            if row["type"] == "true":
                row["type"] = Types.REAL
                real_rows.append(row)
            elif row["type"] == "false":
                row["type"] = Types.FAKE
                fake_rows.append(row)
        RD.shuffle(fake_rows)
        RD.shuffle(real_rows)
        fake_rows=fake_rows[:2000]
        real_rows=real_rows[:2000]
        rows = fake_rows + real_rows
        RD.shuffle(rows)
        df = pd.DataFrame(rows)
        df["content"] = df["content"].apply(process_text)
        df["title"] = df["title"].apply(process_text)
        df = df.reset_index(drop=True)
        self.liar_in = df[["content","title"]]
        self.liar_out = df["type"]

    def init_default(self):
        self.init_balanced(10000)

    def init_with_fraction(self, f):
        assert(0 < f <= 1)
        size = self.get_db_size()
        n = int(f * size)
        self.init_with_n_samples(n)

    def get_stats(self, df_in, df_out):
        assert(len(df_in) == len(df_out))
        print(" total: {}, fake: {}%, real: {}%".format(
            len(df_out),
            round((df_out == Types.FAKE).sum() / len(df_out) * 100, 1),
            round((df_out == Types.REAL).sum() / len(df_out) * 100, 1)
        ))
        if 'content' in df_in:
            print(" content with len >= 100: {}%".format(
                round((df_in['content'].str.len() >= 100).sum() / len(df_in) * 100, 1)
            ))
            print(" average content length: {}".format(
                df_in['content'].str.len().sum() / len(df_in)
            ))
    
    def get_all_stats(self): 
        print("=====Data-Container=====")
        print("training data:")
        self.get_stats(self.train_in, self.train_out)
        print("validation data:")
        self.get_stats(self.val_in, self.val_out)
        print("testing data:")
        self.get_stats(self.test_in, self.test_out)
        if self.liar_in is not None:
            print("liar data:")
            self.get_stats(self.liar_in, self.liar_out)
        print("========================")

    def init_balanced(self, half):
        # n = 2 * half
        all_rows = query(
            """
            select setseed({});
            select {}, type from training_articles
            """
            .format(
                DB_GENERATED_SEED,
                ",".join(col for col in self.columns)
            )
        )
        fake_rows = []
        real_rows = []
        for row in all_rows:
            if row[-1] == Types.FAKE: fake_rows.append(row)
            elif row[-1] == Types.REAL: real_rows.append(row)
            else: assert(0)
        assert(half <= len(fake_rows) and half <= len(real_rows))
        RD.shuffle(fake_rows)
        RD.shuffle(real_rows)
        fake_rows=fake_rows[:half]
        real_rows=real_rows[:half]
        rows = fake_rows + real_rows
        RD.shuffle(rows)
        self.init_with_data(rows, 2 * half)

    def init_with_n_samples(self, n):
        total_n = self.get_db_size()
        assert(n <= total_n)
        prop = min(1.0, max(n / total_n * 3, 0.01))
        # only select a small subset of the database for performance
        rows = query(
            """
            select setseed({});
            select {}, type from training_articles where random() < {}
            """
            .format(
                DB_GENERATED_SEED,
                ",".join(col for col in self.columns),
                prop
            )
        )
        self.init_with_data(rows, n)

    def init_with_data(self, rows, n):
        # then shuffle the rows and extract the wanted number of rows
        all_articles = pd.DataFrame(rows, columns = self.columns + ["type"])
        test_size = int(0.2 * n)
        val_size = int(0.2 * n)
        train_size = n - test_size - val_size
        trainval_data, test_data = train_test_split(
            all_articles,
            test_size=test_size,
            train_size=train_size + val_size,
            random_state=GENERATE_SEED,
            shuffle=True,
        )
        train_data, val_data = train_test_split(
            trainval_data,
            test_size=val_size,
            train_size=train_size,
            random_state=GENERATE_SEED,
            shuffle=True,
        )
        train_data.reset_index(inplace=True)
        val_data.reset_index(inplace=True)
        test_data.reset_index(inplace=True)

        # then split into output,input tables
        self.train_in, self.train_out = train_data[self.columns], train_data["type"]
        self.val_in, self.val_out = val_data[self.columns], val_data["type"]
        self.test_in, self.test_out = test_data[self.columns], test_data["type"]
