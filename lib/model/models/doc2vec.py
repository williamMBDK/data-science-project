import pandas as pd
from lib.data import Types
import numpy as np
from sklearn.cluster import KMeans
import itertools
from .word2vec import Word2vec
from .helpers import get_all_words, get_words, normalize_freq, get_n_grams, get_all_n_grams, filter_by_relevance

class Doc2vecwords:
    def __init__(self, df : pd.DataFrame, dimensions):
        self.dimensions = dimensions
        self.total_dimensions = dimensions
        print("get word2vec dataset")
        self.w2v = Word2vec()
        self.w2v.init_google_news()
        print("get unique words")
        words = get_all_words(df)
        print("get word vectors")
        self.wordvectors = self.get_word_vectors(words)
        print("running k-means clustering")
        self.cluster_centers = KMeans(
            n_clusters = dimensions,
            random_state=0
        ).fit(self.wordvectors).cluster_centers_
        self.wordcache = {}

    def get_word_vector(self, word):
        if not word in self.w2v: return None
        return self.w2v[word]

    def get_word_vectors(self, words):
        return np.array(
            list(map(self.get_word_vector,
                filter(lambda word: word in self.w2v, words)
            ))
        )

    def get_closest_center_idx(self, word_vec):
        best_idx = 0
        best_dist = np.inf
        for idx, center in enumerate(self.cluster_centers):
            diff = center - word_vec
            dist = np.inner(diff, diff)
            if dist <= best_dist:
                best_dist = dist
                best_idx = idx
        return best_idx

    def get_vec(self, content, normalize=True):
        words = get_words(content)
        vec = [0] * self.dimensions
        for word in words:
            idx = None
            if word in self.wordcache: idx = self.wordcache[word]
            else:
                word_vec = self.get_word_vector(word)
                if word_vec is None: continue
                idx = self.get_closest_center_idx(word_vec)
                self.wordcache[word] = idx
            vec[idx] += 1
        if normalize: return normalize_freq(vec)
        else: return vec

class Doc2vecgrams:

    def __init__(self, df : pd.DataFrame, dimensions, max_n_grams, use_relevant=False):
        self.max_n_grams = max_n_grams
        self.dimensions = dimensions
        print("get word2vec dataset")
        self.w2v = Word2vec()
        self.w2v.init_google_news()
        print(len(self.w2v.vectors))
        print("get unique words")
        words = filter_by_relevance(df["content"],0.01,0.2) if use_relevant else get_all_words(df)
        print("get word vectors")
        wordvectors = self.get_word_vectors(words)
        print(len(wordvectors))
        print("running k-means clustering")
        self.cluster_centers = KMeans(
            n_clusters = dimensions,
            random_state=0
        ).fit(wordvectors).cluster_centers_
        print("generate 1to{}-grams indices in output array".format(max_n_grams))
        self.generate_indices()
        self.word_idx = {}
        self.n_gram_idx = {}
        #print("get indices of all words")
        # self.word_idx = {}
        # for word in words:
        #     word_vec = self.get_word_vector(word)
        #     if word_vec is not None:
        #         self.word_idx[word] = self.get_closest_center_idx(word_vec)
        # print("finding 1to{}-grams keys".format(max_n_grams))
        # self.n_grams_idxs = {}
        # for n in range(1, max_n_grams + 1):
        #     # n = 1 is just words
        #     n_grams = get_all_n_grams(df, n)
        #     for gram in n_grams:
        #         idx = self.get_n_gram_idx(gram)
        #         if idx != None:
        #             self.n_grams_idxs[gram] = idx

    def generate_indices(self):
        self.idx = {}
        idx = 0
        for n in range(1,self.max_n_grams + 1):
            vals = itertools.product(list(range(self.dimensions)), repeat=n)
            for v in vals:
                self.idx[v] = idx
                idx += 1
        self.total_dimensions = idx

    def get_word_vector(self, word):
        if not word in self.w2v: return None
        return self.w2v[word]

    def get_word_vectors(self, words):
        return np.array(
            list(map(self.get_word_vector,
                filter(lambda word: word in self.w2v, words)
            ))
        )

    def get_closest_center_idx(self, word_vec):
        best_idx = 0
        best_dist = np.inf
        for idx, center in enumerate(self.cluster_centers):
            diff = center - word_vec
            dist = np.inner(diff, diff)
            if dist <= best_dist:
                best_dist = dist
                best_idx = idx
        return best_idx

    def get_word_idx(self, word):
        if word in self.word_idx: return self.word_idx[word]
        word_vec = self.get_word_vector(word)
        if word_vec is None: return None
        self.word_idx[word] = self.get_closest_center_idx(word_vec)
        return self.word_idx[word]

    def get_n_gram_idx(self, gram):
        if gram in self.n_gram_idx: return self.n_gram_idx[gram]
        words = gram.split()
        key = []
        for word in words:
            word_idx = self.get_word_idx(word)
            if word_idx is None:
                self.n_gram_idx[gram] = None
                return None
            key.append(word_idx)
        self.n_gram_idx[gram] = self.idx[tuple(key)]
        return self.n_gram_idx[gram]

    def get_vec(self, content, normalize=True):
        vec = [0] * self.total_dimensions
        for n in range(1, self.max_n_grams + 1):
            n_grams = get_n_grams(content, n)
            for gram in n_grams:
                idx = self.get_n_gram_idx(gram)
                if idx is None: continue
                vec[idx] += 1
        if normalize: return normalize_freq(vec)
        else: return vec
