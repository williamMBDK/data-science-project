from .config import create_connection

def query(q, connection=None, get_return_value=True):
    given_connection = connection is not None
    if not given_connection:
        connection = create_connection()
    rows = None
    with connection.cursor() as cur:
        cur.execute(q)
        if get_return_value:
            rows = cur.fetchall()
    if not given_connection:
        connection.commit()
        connection.close()
    return rows
