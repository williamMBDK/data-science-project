import pandas as pd
from lib.data import Types
import numpy as np

def cnt_type(output : pd.Series, type):
    return int((output == type).sum())

def unique_words(content):
    return list(dict.fromkeys(content.split()))

def get_freq(content, total, wordidx):
    freq = [0] * total
    for word in content.split():
        if word not in wordidx: continue
        idx = wordidx[word]
        freq[idx] += 1
    return freq

# TODO: use this
def normalize_freq(freq):
    res = np.array(freq)
    div = max(sum(res), 1)
    return res / div

# TODO: use this
def get_binary_labels(output : pd.Series):
    y = []
    for _, type in output.items():
        if type == Types.REAL:
            y.append(1.0)
        else:
            y.append(0.0)
    return y

# TODO: use this everywhere
def get_words(content : str):
    return list(map(lambda word: word.strip(), content.split()))

def get_all_words(df : pd.DataFrame):
    words = set()
    for _, row in df.iterrows():
        words.update(get_words(row['content']))
    return list(words)

def get_n_grams(content : str, n : int):
    words = get_words(content)
    total = len(words)
    n_grams = [None] * (total - n + 1)
    for i in range(total - n + 1):
        n_grams[i] = " ".join(words[i:i+n]) # type: ignore
    return n_grams

def get_all_n_grams(df : pd.DataFrame, n : int):
    n_grams = set()
    for _, row in df.iterrows():
        n_grams.update(get_n_grams(row['content'], n))
    return list(n_grams)

def filter_by_relevance(contents: pd.Series, lo=0.05,hi=0.2):
    words = set()
    for content in contents:
        words.update(get_words(content))
    

    
    word_frequencies = {}
    for content in contents:
        for word in set(get_words(content)):
            if word not in word_frequencies:
                word_frequencies[word] = 0
            word_frequencies[word] += 1
    

    avg_number_per_article = []
    for word in words:
        avg_number_per_article.append((word_frequencies[word]/len(contents),word))
    
    avg_number_per_article.sort(reverse=True)
    
    words = []
    for avg_count, word in avg_number_per_article:
        if lo <= avg_count <= hi:
            words.append(word)
    return words
