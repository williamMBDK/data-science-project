import pandas as pd
from sklearn.ensemble import RandomForestClassifier
import numpy as np

from ..model import Model
from lib.data import Types
from .helpers import cnt_type, get_freq

# TODO: idea: divide training data into small portions and take the average/best results. Thus the freq vectors wont get too large.
# TODO: idea: clustering and then for each cluster a random forest. That way the freq vectors wont get too large.

class RandomForestFreqModel(Model):

    def __init__(self):
        self.model = None
        self.total = 0
        self.wordidx = {}

    def getname(self):
        return "RandomForestFreqModel"

    def predict(self, input : pd.Series):
        assert(self.model != None)
        freq = np.array(get_freq(input['content'], self.total, self.wordidx))
        freq = freq.reshape(1,-1)
        prediction = self.model.predict(freq)
        if prediction >= 0.5: return Types.REAL
        else: return Types.FAKE

    def train(self, input : pd.DataFrame, output : pd.Series):
        cnt = {}
        for idx, row in input.iterrows():
            for word in row['content'].split():
                if word not in cnt: cnt[word] = 0
                cnt[word] += 1

        frequest_words = []
        for word in cnt:
            if 10 <= cnt[word] <= 50:
                frequest_words.append(word)

        self.wordidx = {}
        idx = 0
        for word in frequest_words:
            self.wordidx[word] = idx
            idx += 1

        self.total = idx
        X = []
        y = []
        for idx, row in input.iterrows():
            X.append(np.array(get_freq(row['content'], self.total, self.wordidx)))
            if output[idx] == Types.REAL:
                y.append(1.0)
            else:
                y.append(0.0)

        X = np.array(X)
        y = np.array(y).reshape(len(y))
        self.model = RandomForestClassifier(max_depth=100, n_estimators=30).fit(X,y)
