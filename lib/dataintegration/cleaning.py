import numpy as np
import re
import pandas as pd
from cleantext import clean

DATA_FILES_LOCATION = "data/"
NULL="null"

def remove_column(df, column_name):
   df.pop(column_name)

# PROCESSING TEXT

#def rename_column_nan_datatype(df, column):
#    df[column] = df[column].replace([float('nan')], 'unknown')

def replace_num_date_email_url(text):
    text = re.sub(r"[^\s]+@[^\s]+\.[^\s]+", "<EMAIL>", text)
    text = re.sub(r"[^\s]+\.[^\s]+", "<URL>", text)
    months = "(januari|februari|march|april|may|june|juli|august|septemb|octob|novemb|decemb)"
    text = re.sub(r"(\d+.{1,5}?%s|%s.{1,3}\d+[^\s]*)" % (months, months), "<DATE>", text)
    # text = re.sub(r"\s\d+[\.,]?\d*", " <NUM>", text)
    text = re.sub(r"[^\w\d\s]", '', text)
    return text

def remove_commas(text):
    text = re.sub(r"[,]", '', text)
    return text

def process_text(text):
    if type(text) == type(0.0):
        return ""
    cleaned_text = clean(
        text, 
        clean_all=False,
        extra_spaces=True,
        stemming=False,
        stopwords=True,
        lowercase=True,
        punct=True,
        numbers=True,
        stp_lang="english"
    )
    cleaned_text = replace_num_date_email_url(cleaned_text)
    cleaned_text = remove_commas(cleaned_text)
    return cleaned_text

# PROCESSING LISTS

BAD_VALS = set(["unknown", "0.0", "0", "",NULL,""," ","\n"])
def process_list(lst, split_on_space = False):
    cleaned = None
    if split_on_space:
        cleaned = lst.replace(',',' ').replace('\t', ' ').replace('\n',' ').split(' ')
    else:
        cleaned = lst.split(',')
    cleaned = list(map(lambda s: ' '.join(s.split()).strip(), cleaned))
    cleaned = list(map(lambda s: s.lower(), cleaned))
    cleaned = list(filter(lambda s: s not in BAD_VALS, cleaned))
    cleaned = list(map(lambda s: process_text(s), cleaned))
    cleaned = list(filter(lambda s: s not in BAD_VALS, cleaned))
    cleaned = list(dict.fromkeys(cleaned))
    return ",".join(cleaned)

def clean_fakenewscorpus(df : pd.DataFrame):
    print("=====FakeNewsCorpusCleaning=====")
    df.drop(df.columns[0], axis=1, inplace=True) # drop the column 0 since indexes is built in to pandas dataframes

    """ 
    # remove useless columns
    remove_column(df, "keywords")
    remove_column(df, "summary")
    remove_column(df, "meta_description")
    remove_column(df, "tags")
    remove_column(df, "meta_keywords")
    remove_column(df, "scraped_at")
    remove_column(df, "inserted_at")
    remove_column(df, "updated_at")
    remove_column(df, "source")

    # removing 'nan'
    rename_column_nan_datatype(df, 'type')
    rename_column_nan_datatype(df, 'authors')

    # remove rows with nan
    df.dropna(how='any', axis=0, inplace=True)
    """

    # remove useless columns
    print("removing columns")
    remove_column(df, "id")
    remove_column(df, "inserted_at")
    remove_column(df, "updated_at")
    remove_column(df, "source")

    # handle NaN
    print("removing NaN")
    df.replace(to_replace=np.nan, value=NULL, inplace=True)

    # handle lists
    print("cleaning keywords")
    df["keywords"] = df["keywords"].apply(process_list)
    print("cleaning tags")
    df["tags"] =df["tags"].apply(lambda s: process_list(s, True))
    print("cleaning metakeywords")
    df["meta_keywords"] =df["meta_keywords"].apply(process_list)
    print("cleaning authors")
    df["authors"] = df["authors"].apply(process_list)

    # apply processing to the columns which contain natural language
    print("cleaning content")
    df['content'] = df['content'].apply(process_text)
    print("cleaning title")
    df['title'] = df['title'].apply(process_text)
    print("cleaning meta_description")
    df['meta_description'] = df['meta_description'].apply(process_text)
    print("cleaning summary")
    df['summary'] = df['summary'].apply(process_text)

    print("================================")
    
    return df

def clean_wikinewscorpus(df: pd.DataFrame):
    df.drop(df.columns[0], axis=1, inplace=True) # drop the column 0 since indexes is built in to pandas dataframes

    for idx, row in df.iterrows():
        if not row["date"][0].isdigit():
            df.at[idx, "date"] = NULL

    # REPLACE NANS WITH NULL

    df.replace(to_replace=np.nan, value=NULL, inplace=True)
    df['content'] = df['content'].apply(process_text)
    df['title'] = df['title'].apply(process_text)

    return df

def clean_data_from_file_fakenewscorpus(filename):
    df = pd.read_csv(DATA_FILES_LOCATION+filename, low_memory=False)
    df = clean_fakenewscorpus(df)
    return df

def clean_data_from_file_wikinewscorpus(filename="wikinews.csv"):
    df = pd.read_csv(DATA_FILES_LOCATION+filename)
    df = clean_wikinewscorpus(df)
    return df
