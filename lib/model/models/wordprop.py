import pandas as pd

from ..model import Model
from lib.data import Types
from .helpers import cnt_type

class WordProp(Model):

    def __init__(self):
        self.real_words = set()
        self.fake_words = set()

    def getname(self):
        return "WordProp"

    def predict(self, input : pd.Series):
        real_cnt = 0
        fake_cnt = 0
        for word in input['content'].split():
            if word in self.real_words: real_cnt += 1
            elif word in self.fake_words: fake_cnt += 1
        if real_cnt > fake_cnt: return Types.REAL
        else: return Types.FAKE

    def train(self, input : pd.DataFrame, output : pd.Series):
        self.real_words = set()
        self.fake_words = set()
        freq_fake = {}
        freq_real = {}
        for idx, row in input.iterrows():
            for word in row['content'].split():
                if not word in freq_fake:
                    freq_fake[word] = 0
                    freq_real[word] = 0
                if output[idx] == Types.FAKE:
                    freq_fake[word]+=1
                else:
                    freq_real[word]+=1
        fake = cnt_type(output, Types.FAKE)
        real = cnt_type(output, Types.REAL)
        for word in freq_fake:
            p_fake = freq_fake[word]/fake # normalize
            p_real = freq_real[word]/real # normalize
            if p_fake > p_real:
                self.fake_words.add(word)
            else:
                self.real_words.add(word)
