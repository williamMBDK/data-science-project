import pandas as pd

class Model:
    def getname(self):
        raise NotImplemented("Model.getname must be implemented")

    def __str__(self):
        return "".format(self.getname())

    def predict(self, input : pd.Series):
        """
        input is a series of the form
            {
             'title' : "hey",
             'content' : "hey",
             'type' : TYPES.
            }
        """
        raise NotImplemented("Model.predict must be implemented (types.FAKE | types.REAL)")

    def train(self, input : pd.DataFrame, output : pd.Series):
        raise NotImplemented("Model.train must be implemented")
