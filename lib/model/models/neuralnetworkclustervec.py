import pandas as pd
from sklearn.neural_network import MLPClassifier
import numpy as np

from ..model import Model
from lib.data import Types
from .helpers import get_binary_labels 
from .doc2vec import Doc2vecwords, Doc2vecgrams

class NeuralNetworkClusterVec(Model):

    def __init__(self, use_n_grams):
        self.model = None
        self.use_n_grams = use_n_grams

    def getname(self):
        return "NeuralNetworkClusterVec(" + ("ngrams" if self.use_n_grams else "words") + ")"

    def predict(self, input : pd.Series):
        assert(self.model != None)
        vec = self.d2v.get_vec(input['content'])
        vec = vec.reshape(1,-1)
        prediction = self.model.predict(vec)
        if prediction[0] >= 0.5: return Types.REAL
        else: return Types.FAKE

    def train(self, input : pd.DataFrame, output : pd.Series):
        self.d2v = Doc2vecgrams(input, 50, 2) if self.use_n_grams else Doc2vecwords(input, 200)
        print("getting vectors")
        X = np.array([self.d2v.get_vec(row['content']) for _, row in input.iterrows()])
        print("getting labels")
        y = np.array(get_binary_labels(output))
        print("training neural network")
        self.model = MLPClassifier(hidden_layer_sizes=(500,100,30), max_iter=1000, random_state=1).fit(X,y)
