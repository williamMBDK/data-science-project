from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from .config import create_connection, DB_STD

def create_database(db = DB_STD):
    print("Configuration is:", db)
    ans = input("Want to continue? [y/N]: ")
    if ans == "y":
        conn = create_connection(db, False)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT) # needed to create databases
        with conn.cursor() as cur:
            cur.execute(
                sql.SQL("CREATE DATABASE {}").format(sql.Identifier(db["database"])),
            )

# TODO: text instead of varchar?

def create_tables():
    conn = create_connection()
    with conn.cursor() as cur:
        cur.execute(
            # bigserial creates an auto-increment integer non-null column with up to 2^64 bit values
            # CREATE TABLE IF NOT EXISTS Articles (
            """
            CREATE TABLE IF NOT EXISTS articles_fncorpus (
                article_id SERIAL,
                type VARCHAR(255),
                title VARCHAR(4095),
                url VARCHAR(4095),
                domain VARCHAR(255),
                content VARCHAR NOT NULL,
                scraped_at TIMESTAMP,
                meta_description TEXT,
                summary TEXT,
                PRIMARY KEY (article_id)
            )
            """
        )
        # CONSTRAINT TitleDomainUnique UNIQUE (title, domain)
        # url UNIQUE -> dr.dk/nyheder has many news on the same url
    with conn.cursor() as cur:
        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS authors_fncorpus (
                author_id SERIAL,
                name TEXT NOT NULL UNIQUE,
                PRIMARY KEY (author_id)
            )
            """
        )
    with conn.cursor() as cur:
        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS has_author_fncorpus (
                article_id INT REFERENCES articles_fncorpus (article_id)
                    ON DELETE CASCADE
                    ON UPDATE CASCADE
                    DEFERRABLE INITIALLY DEFERRED,
                author_id INT REFERENCES authors_fncorpus(author_id)
                    ON DELETE CASCADE
                    ON UPDATE CASCADE
                    DEFERRABLE INITIALLY DEFERRED,
                PRIMARY KEY (article_id, author_id)
            )
            """
        )
    # TODO: set ON DELETE/ON UPDATE on the other relations
    with conn.cursor() as cur:
        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS articles_wikinews (
                article_id SERIAL PRIMARY KEY,
                title TEXT,
                date TIMESTAMP,
                url TEXT,
                content TEXT
            )
            """
        )
    with conn.cursor() as cur:
        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS keywords_fncorpus (
                keyword_id SERIAL PRIMARY KEY,
                keyword TEXT
            )
            """
        )
    with conn.cursor() as cur:
        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS has_keyword_fncorpus (
                article_id INT REFERENCES articles_fncorpus (article_id),
                keyword_id INT REFERENCES keywords_fncorpus (keyword_id),
                PRIMARY KEY (article_id, keyword_id)
            )
            """
        )
    with conn.cursor() as cur:
        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS metakeywords_fncorpus (
                metakeyword_id SERIAL PRIMARY KEY,
                metakeyword TEXT
            )
            """
        )
    with conn.cursor() as cur:
        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS has_metakeyword_fncorpus (
                article_id INT REFERENCES articles_fncorpus (article_id),
                metakeyword_id INT REFERENCES metakeywords_fncorpus (metakeyword_id),
                PRIMARY KEY (article_id, metakeyword_id)
            )
            """
        )
    with conn.cursor() as cur:
        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS tags_fncorpus (
                tag_id SERIAL PRIMARY KEY,
                tag TEXT
            )
            """
        )
    with conn.cursor() as cur:
        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS has_tag_fncorpus (
                article_id INT REFERENCES articles_fncorpus (article_id),
                tag_id INT REFERENCES tags_fncorpus (tag_id),
                PRIMARY KEY (article_id, tag_id)
            )
            """
        )
    with conn.cursor() as cur:
        cur.execute(
            """
            CREATE VIEW articles(fakenews_id, wikinews_id, title, url, domain, type, content) AS
                SELECT article_id, NULL, title, url, domain, type, content
                FROM articles_fncorpus
            UNION ALL 
                SELECT NULL, article_id, title, url, 'wikinews.org', 'REAL', content
                FROM articles_wikinews
            """
        )
    with conn.cursor() as cur:
        cur.execute(
            """
            CREATE VIEW training_articles AS
                SELECT title,url,content,'REAL' as type
                FROM articles 
                WHERE (type = 'reliable' OR type = 'political' or type = 'REAL')
                      and content is not null and content != ''
            UNION ALL
                SELECT title,url,content,'FAKE' as type 
                FROM articles 
                WHERE (type = 'junksci'
                    OR type = 'conspiracy' OR type = 'fake') and content is not null and content != ''
            """
        )
    conn.commit()
    conn.close()

def set_table_to_csv_data(filename, tablename):
    conn = create_connection()
    with conn.cursor() as cur:
        cur.execute(
            sql.SQL("""
                COPY {}
                FROM %s
                DELIMITER ',' CSV HEADER;
            """)
            .format(sql.Identifier(tablename)),
            [filename]
        )
    conn.commit()
    conn.close()

def delete_database(db = DB_STD):
    print("Configuration is:", db)
    ans = input("Want to continue deleting database? [y/N]: ")
    if ans == "y":
        conn = create_connection(db, False)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT) # needed to create databases
        with conn.cursor() as cur:
            cur.execute(
                sql.SQL("DROP DATABASE {}").format(sql.Identifier(db["database"])),
            )
