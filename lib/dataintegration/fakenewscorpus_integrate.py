import pandas as pd
from .cleaning import clean_data_from_file_fakenewscorpus

def make_db_csvs_fakenewscorpus(filename):
    df = clean_data_from_file_fakenewscorpus(filename)
    df_to_db_csvs_fakenewscorpus(df)

def df_to_db_csvs_fakenewscorpus(df):

    print("=====DataIntegration-FakeNewsCorpus=====")

    #articles to csv
    print("articles table csv")
    articles_df = df[["type", "title", "url", "domain", "content", "scraped_at", "meta_description", "summary"]] 
    articles_df.to_csv("./data/articles_fakenews.csv", index=True, index_label="article_id", header=True)

    # the rest of the csvs
    print("authors and has_author csv")
    create_relations_for_attr(df, "authors", "name", "author_id", "authors", "has_author")
    print("keywords and has_keyword csv")
    create_relations_for_attr(df, "keywords", "keyword", "keyword_id", "keywords", "has_keyword")
    print("meta_keywords and has_metakeyword csv")
    create_relations_for_attr(df, "meta_keywords", "metakeyword", "metakeyword_id", "metakeywords", "has_metakeyword")
    print("tags and has_tag csv")
    create_relations_for_attr(df, "tags", "tag", "tag_id", "tags", "has_tag")
    print("========================================")

# extracts an attribute and creates a many-to-many 
# relationship between articles and that attribute.
# Two new csv files are created. 
def create_relations_for_attr(df, column_name_df, column_name_db, id_column_name_db, table_name, relation_table_name):

    attrs = set()
    attrs_for_articles = {}

    # this is a pandas antipattern, but it is needed, how do we improve this?
    for article_id, row in df.iterrows():
        attr_lst = row[column_name_df].split(",")
        for attr in attr_lst:
            if attr != '':
                attrs.add(attr)
        attrs_for_articles[article_id] = attr_lst

    attrs_df = pd.DataFrame(list(attrs), columns=[column_name_db])
    attr_csv = "./data/"+table_name+"_fakenews.csv"
    attrs_df.to_csv(attr_csv, index=True, index_label=id_column_name_db, header=True)

    attr_ids = {}
    for attr_id, row in attrs_df.iterrows():
        attr_ids[row[column_name_db]] = attr_id

    has_attr = []
    for article_id in attrs_for_articles:
        for attr in attrs_for_articles[article_id]:
            if attr != '':
                attr_id = attr_ids[attr]
                has_attr.append((article_id, attr_id))

    has_attr_df = pd.DataFrame(list(has_attr), columns=["article_id", id_column_name_db])
    has_attr_csv = "./data/"+relation_table_name+"_fakenews.csv"
    has_attr_df.to_csv(has_attr_csv, index=False, header=True)
