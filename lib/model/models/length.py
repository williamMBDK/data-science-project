import pandas as pd
import numpy as np

from ..model import Model
from lib.data import Types

from .helpers import get_binary_labels
from sklearn.linear_model import LogisticRegression
from sklearn import svm

class LogisticRegressionLength(Model):

    def getname(self):
        return "LogisticRegressionLength"

    def avg_word_len(self, content):
      
      return len(list(filter(lambda word: len(word.strip()) > 7, content.split(' '))))

      word_lengths = list(map(lambda word: len(word.strip()), content.split(' ')))
      return sum(word_lengths)/len(word_lengths)
        

    def predict(self, input : pd.Series):
      #return Types.FAKE
      length = len(input['content'])

      avg_word_len = self.avg_word_len(input['content'])
      f_input = np.array([[avg_word_len, length]])
      print(f_input)
      
      prediction = self.model.predict(f_input)
      if prediction < 0.5:
        print("Predicted fake")
      #print(prediction)
      if prediction >= 0.5: return Types.REAL
      else: return Types.FAKE

    def train(self, input : pd.DataFrame, output : pd.Series):
      X = []
      for row in input['content']:
        avg_word_length = self.avg_word_len(row)
        X.append([avg_word_length, len(row)])
      X = np.array(X)

      y = np.array(get_binary_labels(output))
      #print(y)
      #print(y)
      print(X)
      print(y)
      self.model = svm.SVC().fit(X,y)
      
