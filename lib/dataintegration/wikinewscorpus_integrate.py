import pandas as pd
from .cleaning import clean_data_from_file_wikinewscorpus

def make_db_csvs_wikinewscorpus(filename):
    df = clean_data_from_file_wikinewscorpus(filename)
    df_to_db_csvs_wikinewscorpus(df)

def df_to_db_csvs_wikinewscorpus(df):
    articles_without_id = df.drop(['id'], axis=1)
    articles_without_id.to_csv("./data/articles_wikinews.csv", index=True, index_label="article_id")
    