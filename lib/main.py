import pandas as pd
import sys
import os
import warnings
import time

from lib.model.models.length import LogisticRegressionLength
# warnings.filterwarnings("error")

from .scraping import WikinewsScraper
from .dataintegration.cleaning import clean_data_from_file_fakenewscorpus, clean_fakenewscorpus
from .data import extract_subset_of_data, DataContainer, Types
from .db import config, create_database, create_tables, load_all_datasets, delete_database as deletedatabase_db, query, create_connection
from .dataintegration.fakenewscorpus_integrate import make_db_csvs_fakenewscorpus
from .dataintegration.wikinewscorpus_integrate import make_db_csvs_wikinewscorpus
from .model import ModelEvaluator, run_model_pipeline
from .model.models import AlwaysFake, AlwaysReal, WordProp, LogisticRegressionFreqModel, RandomForestFreqModel, NaiveBayes, RandomForestClusterVec, NeuralNetworkClusterVec, NaiveBayesClusterVec, NeuralNetworkAvgVec, DistanceW2V
from .model.models.helpers import get_words
from .model.models.word2vec import Word2vec
from .model.models.doc2vec import Doc2vecwords

### DATA ###
def scrape_wikinews():
    os.makedirs('./data', exist_ok=True)
    group_nr=34
    letters_to_check = "ABCDEFGHIJKLMNOPRSTUVWZABCDEFGHIJKLMNOPRSTUVWZ"[group_nr%23:group_nr%23+10]
    scraper = WikinewsScraper()
    df = scraper.run(letters_to_check)
    df.to_csv("./data/wikinews.csv")
    print(df)
    return df

def clean_samples():
    # repo: https://github.com/several27/FakeNewsCorpus
    df = pd.read_csv("https://raw.githubusercontent.com/several27/FakeNewsCorpus/master/news_sample.csv")
    df = clean_fakenewscorpus(df)
    print(df)
    return df

def clean_data_fakenewscorpuslarge():
    # repo: https://github.com/several27/FakeNewsCorpus
    clean_data_from_file_fakenewscorpus("fakenews_large.csv")

def clean_data_fakenewscorpussmall():
    # repo: https://github.com/several27/FakeNewsCorpus
    clean_data_from_file_fakenewscorpus("fakenews_small.csv")


def build_verylarge_dataset_fakenewscorpus():
    filename=sys.argv[2]
    os.makedirs('./data', exist_ok=True)
    df = extract_subset_of_data(filename, 0.01)
    df.to_csv("./data/fake_news_verylarge.csv")

def build_large_dataset_fakenewscorpus():
    filename=sys.argv[2]
    os.makedirs('./data', exist_ok=True)
    df = extract_subset_of_data(filename, 0.001)
    df.to_csv("./data/fake_news_large.csv")

def build_small_dataset_fakenewscorpus():
    filename=sys.argv[2]
    os.makedirs('./data', exist_ok=True)
    df = extract_subset_of_data(filename, 0.0001)
    df.to_csv("./data/fake_news_small.csv")

### DATABASE ###

def setupdatabase():
    create_database()
    create_tables()

def dataintegration_fakenewscorpussmall():
    filename="fakenews_small.csv"
    make_db_csvs_fakenewscorpus(filename)

def dataintegration_fakenewscorpuslarge():
    filename="fakenews_large.csv"
    make_db_csvs_fakenewscorpus(filename)

def dataintegration_fakenewscorpusverylarge():
    filename="fakenews_verylarge.csv"
    make_db_csvs_fakenewscorpus(filename)

def dataintegration_wikinewscorpus():
    filename="wikinews.csv"
    make_db_csvs_wikinewscorpus(filename)

def load_all_data():
    load_all_datasets()

def deletedatabase():
    deletedatabase_db()

### MODEL ###

def final_testing():
    if input() != "run": return
    models = [
        AlwaysFake(),
        AlwaysReal(),
        LogisticRegressionFreqModel(),
        NaiveBayes(),
        NaiveBayes(['content', 'title']),
        NaiveBayesClusterVec(False),
        NaiveBayesClusterVec(True),
        NeuralNetworkAvgVec(),
        NeuralNetworkClusterVec(False),
        NeuralNetworkClusterVec(True),
        RandomForestClusterVec(False),
        RandomForestClusterVec(True),
        RandomForestFreqModel(),
        WordProp(),
    ]
    for model in models:
        print()
        print("TESTING {}".format(model.getname()))
        print()
        data = DataContainer(columns=['content', 'title'])
        data.init_default()
        data.init_liar()
        data.get_all_stats()
        start_training = time.time()
        model.train(data.train_in, data.train_out)
        end_training = time.time()
        print("TRAINING_TIME: {}".format(end_training - start_training))
        evaluator = ModelEvaluator(model)
        start_test = time.time()
        evaluator.init(data.test_in, data.test_out)
        end_test = time.time()
        print("TESTING TIME(fakenewscorpus and wikinews datasets): {}".format(end_test - start_test))
        evaluator.summary("fakenewscorpus and wikinews datasets")
        start_liar = time.time()
        evaluator.init(data.liar_in, data.liar_out)
        end_liar = time.time()
        print("TESTING TIME(liar dataset): {}".format(end_liar - start_liar))
        evaluator.summary("liar dataset")
        del model

def run_alwaysfake_model():
    model = AlwaysFake()
    run_model_pipeline(model)

def run_alwaysreal_model():
    model = AlwaysReal()
    run_model_pipeline(model)

def run_wordprop_model():
    model = WordProp()
    run_model_pipeline(model)

def run_logregfreq_model():
    model = LogisticRegressionFreqModel()
    run_model_pipeline(model)

def run_randomforestfreq_model():
    model = RandomForestFreqModel()
    run_model_pipeline(model)

def run_naivebayes_model():
    model = NaiveBayes()
    run_model_pipeline(model)

def run_naivebayestitle_model():
    model = NaiveBayes(['content', 'title'])
    run_model_pipeline(model, columns=['content', 'title'])

def run_randomforestclustervec_model():
    model = RandomForestClusterVec(False)
    run_model_pipeline(model)

def run_neuralnetworkclustervec_model():
    model = NeuralNetworkClusterVec(False)
    run_model_pipeline(model)

def run_naivebayesclustervec_model():
    model = NaiveBayesClusterVec()
    run_model_pipeline(model)

def run_neuralnetworkavgvec_model():
    model = NeuralNetworkAvgVec()

    run_model_pipeline(model)

def run_distanceW2V_model():
    model = DistanceW2V()
    run_model_pipeline(model)

def run_logreglength_model():
    model = LogisticRegressionLength()
    run_model_pipeline(model)

### TEST ###

def testquery2():
    rows = query("select title from training_articles")
    print(len(rows))
    for row in rows:
        print(row)

def testquery():
    rows = query("select title,type,domain from articles where domain like '%abc%'")
    print(len(rows))
    for row in rows:
        print(row)
    rows = query("select type, count(title) from articles group by type")
    print(len(rows))
    for row in rows:
        print(row)
    print(query("select count(*) from articles"))

def tempviews():
    conn = create_connection()
    with conn.cursor() as cur:
        cur.execute(
            """
            create view training_articles as select * from articles
            """
        )
    conn.commit()
    conn.close()

def test_liar():
    data = DataContainer(["title","content"])
    data.init_default()
    data.init_liar()
    data.get_all_stats()

def test_datahandler():
    handler1 = DataContainer(["title","content"])
    handler1.init_with_n_samples(1000)
    handler1.init_with_fraction(0.5)
    handler1.init_default()
    print(handler1.train_in)
    time.sleep(3)
    handler2 = DataContainer(["title","content"])
    handler2.init_default()
    print(handler1.train_in.compare(handler2.train_in))
    print(handler1.train_out.compare(handler2.train_out))
    print(handler1.val_in.compare(handler2.val_in))
    print(handler1.val_out.compare(handler2.val_out))
    print(handler1.test_in.compare(handler2.test_in))
    print(handler1.test_out.compare(handler2.test_out))

def testmodeleval():
    model = AlwaysFake()
    evaluator = ModelEvaluator(model)
    input = pd.DataFrame({'content' : ['a','b','c','d','e',]})
    output = pd.DataFrame({'type' : [
        Types.FAKE,
        Types.REAL,
        Types.FAKE,
        Types.FAKE,
        Types.FAKE,
    ]})
    evaluator.init(input, output)
    evaluator.summary()

    output = pd.DataFrame({'type' : [
        Types.REAL,
        Types.REAL,
        Types.REAL,
        Types.REAL,
        Types.REAL,
    ]})
    evaluator.init(input, output)
    evaluator.summary()

    output = pd.DataFrame({'type' : [
        Types.FAKE,
        Types.FAKE,
        Types.FAKE,
        Types.FAKE,
        Types.FAKE,
    ]})
    evaluator.init(input, output)
    evaluator.summary()

    output = pd.DataFrame({'type' : [
        Types.REAL,
        Types.REAL,
        Types.REAL,
        Types.FAKE,
        Types.FAKE,
    ]})
    evaluator.init(input, output)
    evaluator.summary()

def test_pipeline():
    model = AlwaysFake()
    run_model_pipeline(model)

def test_word2vec():
    data = DataContainer()
    data.init_balanced(1000)
    w2v = Word2vec()
    w2v.init_glove()
    total = 0
    cnt = 0
    cntword = {}
    for idx, row in data.train_in.iterrows():
        words = get_words(row['content'])
        for word in words:
            if word not in cntword: cntword[word] = 0
            cntword[word] += 1
        for word in words:
            total += 1
            if word in w2v:
                cnt += 1
    print("uniques: ", len(cntword))
    print("in w2v: ", cnt)
    print("total words in all contents: ", total)
    ones = 0
    for word in cntword:
        if cntword[word] == 1:
            ones += 1
    print("ones: ", ones)

def test_doc2vec():
    data = DataContainer()
    data.init_balanced(1000)
    print("hey1")
    d2v = Doc2clustervec(data.train_in, 10)
    print("hey5")
    for _, row in data.train_in.iterrows():
        vec = d2v.get_vec(row['content'])
        print(vec)

