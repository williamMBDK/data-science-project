from configparser import ConfigParser
import psycopg2
import os

# https://www.postgresqltutorial.com/postgresql-python/connect/

def config(filename='database.ini', section='postgresql'):

    if not os.path.exists(filename): filename = "../" + filename

    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)

    # get section, default to postgresql
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return db

DB_STD = config()

def create_connection(db = DB_STD, with_database=True):
    conn = None
    if not with_database:
        if db["password"] and db["password"] != "":
            conn = psycopg2.connect(
                user=db["user"],
                host=db["host"],
                port=db["port"],
                password=db["password"],
            )
        else:
            conn = psycopg2.connect(
                user=db["user"],
                host=db["host"],
                port=db["port"],
            )
    else:
        if db["password"] and db["password"] != "":
            conn = psycopg2.connect(
                user=db["user"],
                host=db["host"],
                port=db["port"],
                password=db["password"],
                database=db["database"],
            )
        else:
            conn = psycopg2.connect(
                user=db["user"],
                host=db["host"],
                port=db["port"],
                database=db["database"],
            )
    return conn
