import pandas as pd
import math
from ..model import Model
from lib.data import Types
from .helpers import cnt_type
from .doc2vec import Doc2vecgrams, Doc2vecwords

class NaiveBayesClusterVec(Model):

    def __init__(self, use_n_grams):
        self.use_n_grams = use_n_grams
        self.prob_fake = None
        self.prob_real = None
        self.num_fake = None
        self.num_real = None

    def getname(self):
        return "NaiveBayesClusterVec(" + ("ngrams" if self.use_n_grams else "words") + ")"

    def predict(self, input : pd.Series):
        real_c_prob = math.log(self.num_real / (self.num_fake + self.num_real))
        fake_c_prob = math.log(self.num_fake / (self.num_fake + self.num_real))
        vec = self.d2v.get_vec(input['content'], False)
        for i in range(self.dimensions):
            #if vec[i] == 0:
            #    real_c_prob -= math.log(self.num_real)
            #    fake_c_prob -= math.log(self.num_fake)
            #    continue
            real_c_prob += self.prob_real[i] * vec[i]
            fake_c_prob += self.prob_fake[i] * vec[i]
        if real_c_prob >= fake_c_prob:
            return Types.REAL
        else:
            return Types.FAKE

    def train(self, input : pd.DataFrame, output : pd.Series):
        self.d2v = Doc2vecgrams(input, 50, 2, False) if self.use_n_grams else Doc2vecwords(input, 200)
        self.dimensions = self.d2v.total_dimensions
        self.prob_fake = [1] * self.dimensions
        self.prob_real = [1] * self.dimensions
        for idx, row in input.iterrows():
            vec = self.d2v.get_vec(row['content'], False)
            for i in range(self.dimensions):
                if vec[i] == 0: continue
                if output[idx] == Types.FAKE:
                    self.prob_fake[i]+=vec[i]
                else:
                    self.prob_real[i]+=vec[i]
        self.num_fake = sum(self.prob_fake)
        self.num_real = sum(self.prob_real)
        for i in range(self.dimensions):
            self.prob_fake[i] = math.log(self.prob_fake[i] / self.num_fake)
            self.prob_real[i] = math.log(self.prob_real[i] / self.num_real)
