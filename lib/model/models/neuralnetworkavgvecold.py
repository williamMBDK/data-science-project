import pandas as pd
from sklearn.neural_network import MLPClassifier
import numpy as np


from ..model import Model
from lib.data import Types
from .helpers import Word2vec, get_words, filter_by_relevance, get_binary_labels


class NeuralNetworkAvgVec(Model):

    def __init__(self):
        self.model = None

    def getname(self):
        return "NeuralNetworkAvgVec"

    def get_average_word_vec(self, content):
        word_vectors = []
        for word in get_words(content):
            if word in self.w2v and word in self.relevant_words: # just ignore words if it's not in the word2vec model
                word_vectors.append(self.w2v[word])
        # watch out, if word_vectors is empty then it returns nan
        if len(word_vectors) == 0:
            return np.zeros((self.dimension))
        return np.mean(word_vectors, axis=0)

    def predict(self, input : pd.Series):
        #return Types.FAKE
        assert(self.model != None)

        avg_vec = self.get_average_word_vec(input['content'])
        avg_vec = avg_vec.reshape(1,-1)
        prediction = self.model.predict(avg_vec)
        if prediction[0] >= 0.5: return Types.REAL
        else: return Types.FAKE

    def train(self, input : pd.DataFrame, output : pd.Series):
        self.relevant_words = set(filter_by_relevance(input['content'],0.0001,0.2))
        print("number of relevant words:", len(self.relevant_words))
        
        print("getting word2vec")
        self.dimension = 300
        self.w2v = Word2vec()
        self.w2v.init_google_news()
        

        print("calculating averages")
        X = []
        for content in input['content']:
            X.append(self.get_average_word_vec(content))
        X = np.array(X)
        y = np.array(get_binary_labels(output))
        print("training neural network")
        self.model = MLPClassifier(hidden_layer_sizes=(400,500,300,100), max_iter=1000, random_state=1).fit(X,y)
        

        """
        self.d2v = Doc2clustervec(input, 200)
        print("getting vectors")
        X = np.array([self.d2v.get_vec(row['content']) for _, row in input.iterrows()])
        print("getting labels")
        y = np.array(get_binary_labels(output))
        print("training neural network")
        self.model = MLPClassifier(hidden_layer_sizes=(150,100,100,100,50), max_iter=1000, activation = 'relu', random_state=1).fit(X,y)
        """