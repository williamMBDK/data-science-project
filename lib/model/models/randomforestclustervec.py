import pandas as pd
from sklearn.ensemble import RandomForestClassifier
import numpy as np

from ..model import Model
from lib.data import Types
from .helpers import get_binary_labels
from .doc2vec import Doc2vecgrams, Doc2vecwords

class RandomForestClusterVec(Model):

    def __init__(self, use_n_grams):
        self.use_n_grams = use_n_grams
        self.model = None

    def getname(self):
        return "RandomForestClusterVec(" + ("ngrams" if self.use_n_grams else "words") + ")"

    def predict(self, input : pd.Series):
        assert(self.model != None)
        vec = self.d2v.get_vec(input['content'])
        vec = vec.reshape(1,-1)
        prediction = self.model.predict(vec)
        if prediction >= 0.5: return Types.REAL
        else: return Types.FAKE

    def train(self, input : pd.DataFrame, output : pd.Series):
        self.d2v = Doc2vecgrams(input, 50, 2) if self.use_n_grams else Doc2vecwords(input, 200)
        print("getting vectors")
        X = np.array([self.d2v.get_vec(row['content']) for _, row in input.iterrows()])
        print("getting labels")
        y = np.array(get_binary_labels(output))
        print("training random forests")
        self.model = RandomForestClassifier(max_depth=100, n_estimators=100).fit(X,y)
