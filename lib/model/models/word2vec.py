import pandas as pd
from lib.data import Types
import warnings
import numpy as np
warnings.resetwarnings()
import gensim.downloader
warnings.filterwarnings("error")
from sklearn.cluster import KMeans

class Word2vec:
    def __init__(self):
        self.vectors = None

    def listmodels(self):
        print(list(gensim.downloader.info()['models'].keys()))

    def init_glove(self):
        self.vectors = gensim.downloader.load('glove-twitter-25')

    def init_google_news(self):
        self.vectors = gensim.downloader.load('word2vec-google-news-300')

    def __getitem__(self, word):
        return self.vectors[word]

    def __contains__(self, word):
        return word in self.vectors

    def closest(self, word, n):
        return self.vectors.most_similar(word, topn=n)
