from .setup import set_table_to_csv_data
from .setup import create_connection
from pandas import read_csv
from .queries import query

import os

def load_all_datasets():

    print("=====Load-data=====")

    def copy_csv_to_table(filename1,filename2):
        print("copying {}".format(filename1))
        # filename is also tablename
        os.system("sudo cp ./data/{}.csv /tmp/{}.csv".format(filename1, filename2))
        os.system("sudo chown postgres /tmp/{}.csv".format(filename2))
        # we need to use \\copy (psql) instead of COPY (sql)
        os.system(
            """ cd /tmp && sudo -u postgres psql -d dreamteam_db -c "\\copy {} from '/tmp/{}.csv' with delimiter as ',' NULL as 'null' csv header;" """
            .format(filename2, filename2)
        )
        print()

    copy_csv_to_table("articles_wikinews","articles_wikinews")
    copy_csv_to_table("articles_fakenews","articles_fncorpus")
    copy_csv_to_table("authors_fakenews","authors_fncorpus")
    copy_csv_to_table("keywords_fakenews","keywords_fncorpus")
    copy_csv_to_table("metakeywords_fakenews","metakeywords_fncorpus")
    copy_csv_to_table("tags_fakenews","tags_fncorpus")
    copy_csv_to_table("has_author_fakenews","has_author_fncorpus")
    copy_csv_to_table("has_keyword_fakenews","has_keyword_fncorpus")
    copy_csv_to_table("has_metakeyword_fakenews","has_metakeyword_fncorpus")
    copy_csv_to_table("has_tag_fakenews","has_tag_fncorpus")

    print("===================")
