import requests
from bs4 import BeautifulSoup
from typing import Optional
import pandas as pd
import concurrent.futures
from threading import Lock
import time

class Requester:

    def __init__(self):
        self.cnt = 0
        self.lock = Lock()

    def get(self, url):

        self.cnt += 1
        print("sending request #" + str(self.cnt))

        self.lock.acquire()
        start_time = time.time()
        response = requests.get(url)
        end_time = time.time()
        self.lock.release()

        print("recieved response: ", end_time - start_time)

        contents = response.text

        return contents

class WikinewsScraperTools:

    requester = Requester()

    @staticmethod
    def get_soup(url):
        contents = WikinewsScraperTools.requester.get(url)
        return BeautifulSoup(contents, 'html.parser')

    @staticmethod
    def get_url(path):
        return "https://en.wikinews.org" + path

class WikinewsArticleLinks:
    def __init__(self, letters_to_check):
        self.letters_to_check = sorted(letters_to_check.upper())
        self.soup : Optional[BeautifulSoup] = None

    def __iter__(self):
        next_url = "https://en.wikinews.org/w/index.php?title=Category:Politics_and_conflicts&from={}".format(self.letters_to_check[0])
        letter_set = set(self.letters_to_check)
        while next_url:
            self.goto_url(next_url)
            for title, url in self.get_article_paths():
                first_letter = WikinewsArticleLinks.get_first_letter(title[0])
                if first_letter and first_letter.upper() in letter_set:
                    yield (title, url)
            next_url = self.get_next_url()

    def goto_url(self, url):
        self.soup = WikinewsScraperTools.get_soup(url)

    def get_article_paths(self):
        assert(self.soup != None)
        container = self.soup.select('#mw-pages')[0].select('.mw-content-ltr')[0]
        for li in container.find_all('li'):
            a = li.find('a')
            title = a.text
            url = WikinewsScraperTools.get_url(a['href'])
            yield (title, url)

    def get_next_url(self):
        assert(self.soup != None)
        containers = self.soup.select('#mw-pages')
        assert(len(containers) == 1)
        container = containers[0]
        for a in container.find_all('a'):
            if "next page" in a.text:
                return WikinewsScraperTools.get_url(a['href'])
        return None

    @staticmethod
    def get_first_letter(s : str):
        for c in s:
            if c.isalpha():
                return c
        return None

class WikinewsScraper:

    _df_index = ["id", "title", "date", "url", "content"]

    def __init__(self):
        pass

    def run(self, letters_to_check) -> pd.DataFrame:

        df = pd.DataFrame(columns=WikinewsScraper._df_index)

        with concurrent.futures.ThreadPoolExecutor(16) as executor:
            futures = []
            id = 1
            for title, url in WikinewsArticleLinks(letters_to_check):
                futures.append(
                    executor.submit(WikinewsScraper.get_article, title, url, id)
                )
                id += 1
            for future in concurrent.futures.as_completed(futures):
                try:
                    series = future.result()
                    if type(series) != "NoneType":
                        df = df.append(series, ignore_index=True)
                except requests.ConnectTimeout:
                    print("ConnectTimeout.")

        return df

    @staticmethod
    def get_article(title, url, id):
        soup = WikinewsScraperTools.get_soup(url)
        try:
            container = soup.select(".mw-parser-output")[0]
            date_p = container.find('p')
            date = date_p.strong.span['title'] if date_p.strong else date_p.text # type: ignore
            ps = date_p.find_next_siblings("p") # type: ignore
            content = ""
            for p in ps:
                if len(content):
                    content += "\n"
                content += p.text
            return pd.Series(
                [id, title,date,url,content],
                index=WikinewsScraper._df_index
            )
        except:
            print(url)
            return None
