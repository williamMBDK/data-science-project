import pandas as pd

from ..model import Model
from lib.data import Types

class AlwaysFake(Model):

    def getname(self):
        return "AlwaysFake"

    def predict(self, input : pd.Series):
        return Types.FAKE

    def train(self, input : pd.DataFrame, output : pd.Series):
        return
