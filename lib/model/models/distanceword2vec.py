import pandas as pd
import math
from ..model import Model
from lib.data import Types
from .helpers import get_words, get_all_words
from .word2vec import Word2vec
import numpy as np
from random import Random

RD = Random()
RD.seed(1234)

class DistanceW2V(Model):

    def __init__(self):
        self.prob_fake = None
        self.prob_real = None
        self.num_fake = None
        self.num_real = None

    def getname(self):
        return "DistanceW2V"

    def predict(self, input : pd.Series):
        print("predicting")
        words = get_words(input["content"])
        print(len(words))
        score_fake = self.getscore(words, self.cnt_fake, self.allwords_fake)
        score_real = self.getscore(words, self.cnt_real, self.allwords_real)
        if score_real >= score_fake: return Types.REAL
        else: return Types.FAKE
        
    def getscore(self, words, cnt_arr, allwords):
        score = 0
        for word1 in words:
            if word1 not in self.w2v: continue
            v1 = np.array(self.w2v[word1])
            for i in range(10):
                idx = RD.randint(0,len(allwords)-1)
                word2 = allwords[idx]
                if word2 not in self.w2v: continue
                v2 = np.array(self.w2v[word2])
                diff = v1 - v2
                dist = np.inner(diff, diff)
                score += 1 / (dist + 1) * cnt_arr[word2]
        return score

    def train(self, input : pd.DataFrame, output : pd.Series):
        print("loading word2vec")
        self.w2v = Word2vec()
        self.w2v.init_google_news()
        print("training")
        self.cnt_fake = {}
        self.cnt_real = {}
        for idx, row in input.iterrows():
            words = get_words(row["content"])
            for word in words:
                if word not in self.w2v: continue
                if output[idx] == Types.REAL:
                    if word not in self.cnt_real: self.cnt_real[word] = 0
                    self.cnt_real[word] += 1
                else:
                    if word not in self.cnt_fake: self.cnt_fake[word] = 0
                    self.cnt_fake[word] += 1
        self.allwords_fake = list(self.cnt_fake.keys())
        self.allwords_real = list(self.cnt_real.keys())
        print(len(self.cnt_fake))
        print(len(self.cnt_real))
