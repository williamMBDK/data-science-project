import pandas as pd

from ..model import Model
from lib.data import Types

class AlwaysReal(Model):

    def getname(self):
        return "AlwaysReal"

    def predict(self, input : pd.Series):
        return Types.REAL

    def train(self, input : pd.DataFrame, output : pd.Series):
        return
