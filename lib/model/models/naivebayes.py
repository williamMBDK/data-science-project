import pandas as pd
import math
from ..model import Model
from lib.data import Types
from .helpers import cnt_type

class NaiveBayes(Model):

    def __init__(self, features_to_use=['content']):
        self.prob_fake = None
        self.prob_real = None
        self.num_fake = None
        self.num_real = None
        self.features_to_use = features_to_use

    def getname(self):
        return "naivebayes({})".format(",".join(self.features_to_use))

    def predict(self, input : pd.Series):
        for feature in self.features_to_use:
            if input[feature] is None:
                return Types.FAKE
        real_c_prob = math.log(self.num_real / (self.num_fake + self.num_real))
        fake_c_prob = math.log(self.num_fake / (self.num_fake + self.num_real))
        words = []
        for feature in self.features_to_use:
            words += input[feature].split()
        uniques = list(dict.fromkeys(words))
        for word in uniques:
            if not word in self.prob_real:
                real_c_prob -= math.log(self.num_real)
                fake_c_prob -= math.log(self.num_fake)
                continue
            real_c_prob += self.prob_real[word]
            fake_c_prob += self.prob_fake[word]
        if real_c_prob >= fake_c_prob:
            return Types.REAL
        else:
            return Types.FAKE

    def train(self, input : pd.DataFrame, output : pd.Series):
        self.prob_fake = {}
        self.prob_real = {}
        
        for idx, row in input.iterrows():
            ok = True
            for feature in self.features_to_use:
                if row[feature] is None:
                    ok = False
            if not ok:
                continue                    
            words = []
            for feature in self.features_to_use:
                words += row[feature].split()
            uniques = list(dict.fromkeys(words))
            for word in uniques:
                if not word in self.prob_fake:
                    self.prob_fake[word] = 1
                    self.prob_real[word] = 1
                if output[idx] == Types.FAKE:
                    self.prob_fake[word]+=1
                else:
                    self.prob_real[word]+=1
    
        self.num_fake = sum(self.prob_fake.values())
        self.num_real = sum(self.prob_real.values())
        
        for word in self.prob_fake:
            self.prob_fake[word] = math.log(self.prob_fake[word] / self.num_fake)
            self.prob_real[word] = math.log(self.prob_real[word] / self.num_real)
