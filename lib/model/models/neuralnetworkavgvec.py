import pandas as pd
from sklearn.neural_network import MLPClassifier
import numpy as np



from ..model import Model
from lib.data import Types
from .helpers import get_words, get_n_grams, filter_by_relevance, get_binary_labels
from .word2vec import Word2vec
# from bintrees import FastRBTree

class NeuralNetworkAvgVec(Model):

    def __init__(self):
        self.model = None

    def getname(self):
        return "NeuralNetworkAvgVec"

    def get_average_word_vec_old(self, content):
        word_vectors = []
        for word in get_words(content):
            if word in self.w2v and word in self.relevant_words: # just ignore words if it's not in the word2vec model
                word_vectors.append(self.w2v[word])
        # watch out, if word_vectors is empty then it returns nan
        if len(word_vectors) == 0:
            return np.zeros((self.dimension))
        return np.mean(word_vectors, axis=0)

    def get_average_word_vec(self, content, n):
        # filter out all irrelevant words from content

        # then we find the list of n-vectors for all n-grams and keep the value with the greatest absolute value at each feature and average them

        words = get_words(content)
        new_content = []
        for word in words:
            if word in self.w2v and word in self.relevant_words:
                new_content.append(word)

        # make d rb-trees

        if len(new_content) < n:
            return np.zeros((self.dimension))

        trees = [set() for _ in range(self.dimension)]
        for i in range(n):
            vec = self.w2v[new_content[i]]
            for d in range(self.dimension):
                trees[d].add((abs(vec[d]), vec[d], i))

        n_gram_vectors = []
        for i in range(n,len(new_content)+1): 
            if i != len(new_content): 
                vec_new = self.w2v[new_content[i]]
            vec_old = self.w2v[new_content[i-n]]
            
            cur = []
            for d in range(self.dimension):
                cur.append(max(trees[d])[1])
                if i != len(new_content):
                    trees[d].add((abs(vec_new[d]), vec_new[d], i))
                trees[d].remove((abs(vec_old[d]), vec_old[d], i-n))
            
            n_gram_vectors.append(cur)
            """
            gram_words = new_content[i:i+n]
            gram_vecs = []
            for word in gram_words:
                gram_vecs.append(self.w2v[word])
            vec = []
            for j in range(self.dimension):
                bst = 0
                for k in range(n):
                    val = gram_vecs[k][j]
                    if abs(val) > abs(bst):
                        bst = val
                vec.append(bst)
            n_gram_vectors.append(vec)
            """
        # watch out, if word_vectors is empty then it returns nan
        return np.mean(np.array(n_gram_vectors), axis=0)

    def predict(self, input : pd.Series):
        #return Types.FAKE
        #assert(self.model != None)

        avg_vec = self.get_average_word_vec_old(input['content'])
        avg_vec = avg_vec.reshape(1,-1)
        counts = [0,0]
        for model in self.models:
            prediction = model.predict(avg_vec)
            counts[round(prediction[0])] += 1
        if counts[1] > counts[0]: return Types.REAL
        else: return Types.FAKE

    def train(self, input : pd.DataFrame, output : pd.Series):
        self.relevant_words = set(filter_by_relevance(input['content'],0.001,0.2))
        print("number of relevant words:", len(self.relevant_words))


        print("getting word2vec")
        self.dimension = 300
        self.w2v = Word2vec()
        self.w2v.init_google_news()
        

        print("calculating averages")
        X = []
        for _, content in enumerate(input['content']):
            X.append(self.get_average_word_vec_old(content))
        X = np.array(X)
        y = np.array(get_binary_labels(output))
        print("training neural networks")

        self.seeds = [483,258,375,847,606,150,601,154,141,927] # random seeds in the range [1,1000] gotten from random.org/integers
        self.models = []
        for i in range(10):
            self.models.append(MLPClassifier(hidden_layer_sizes=(200,100), max_iter=1000, random_state=self.seeds[i]).fit(X,y))
        

        """
        self.d2v = Doc2clustervec(input, 200)
        print("getting vectors")
        X = np.array([self.d2v.get_vec(row['content']) for _, row in input.iterrows()])
        print("getting labels")
        y = np.array(get_binary_labels(output))
        print("training neural network")
        self.model = MLPClassifier(hidden_layer_sizes=(150,100,100,100,50), max_iter=1000, activation = 'relu', random_state=1).fit(X,y)
        """
