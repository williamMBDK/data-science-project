import psycopg2
import csv
import re
import pandas as pd
from cleantext import clean
from bs4 import BeautifulSoup

#
df = pd.read_csv("https://raw.githubusercontent.com/several27/FakeNewsCorpus/master/news_sample.csv")
df.drop(df.columns[0],axis=1,inplace=True) # drop the column 0 since indexes is built in to pandas dataframes
df

def remove_column(df, column_name):
    df.pop(column_name)

# remove useless columns
remove_column(df, "keywords")
remove_column(df, "summary")
remove_column(df, "meta_description")
remove_column(df, "tags")
remove_column(df, "meta_keywords")
remove_column(df, "scraped_at")
remove_column(df, "inserted_at")
remove_column(df, "updated_at")

def rename_column_nan_datatype(df, column, new_name):
    df[column] = df[column].replace([float('nan')], 'unknown')

# replace nans with 'unknown'
rename_column_nan_datatype(df, 'type', 'unknown')
rename_column_nan_datatype(df, 'authors', 'unknown')
def replace_num_date_email_url(text):
    text = re.sub(r"[^\s]+@[^\s]+\.[^\s]+", "<EMAIL>", text)
    text = re.sub(r"[^\s]+\.[^\s]+", "<URL>", text)
    months = "(januari|februari|march|april|may|june|juli|august|septemb|octob|novemb|decemb)"
    text = re.sub(r"(\d+.{1,5}?%s|%s.{1,3}\d+[^\s]*)" % (months, months), "<DATE>", text)
    text = re.sub(r"\s\d+[\.,]?\d*", " <NUM>", text)
    text = re.sub(r"[^\w\d\s]", '', text)
    return text

def process_text(text):
    cleaned_text = clean(
        text, 
        clean_all= False,
        extra_spaces=True,
        stemming=True,
        stopwords=True,
        lowercase=True,
        punct=False,
        numbers=False,
    )
    return replace_num_date_email_url(cleaned_text)


# apply processing to the two columns which contain natural language
df['content'] = df['content'].apply(process_text)
df['title'] = df['title'].apply(process_text)
df
#

def extract_first_row(df):
    print(len(df.iloc[0]))
def extract_rows(df):
    num_cols = df.shape[1]
    num_rows = df.shape[0]
    for i in range(num_rows):
        query = "INSERT INTO testtable2(id,domain,type,url,content,title) VALUES ("
        for j in range(num_cols):
            if not j == num_cols - 1:
                query += str(df.iloc[i][j]) + ","
            else:
                query += str(df.iloc[i][j]) + ")"
    print(query)

extract_rows(df)

    

extract_first_row(df)

con = psycopg2.connect(
    host = "localhost",
    database = "testdb",
    user = "postgres",
    port = 5432,
    password = "postgres"
)

cur = con.cursor()
cur.execute("INSERT INTO testtable(id) VALUES (3)")
cur.execute("SELECT * FROM testtable")
rows = cur.fetchall()
for r in rows:
    print(r[0])

con.close()
#\o/
# |   woo
# |
#/\